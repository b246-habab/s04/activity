# Activity:
# 1. Create an abstract class called Animal that has the following abstract methods:
# - eat(food)
# - make_sound()
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# - Properties:
# 	- Name
# 	- Breed
# 	- Age
# - Methods:
# 	- Getters
# 	- Setters
# 	- Implementation of abstract methods
# 	- call()


# Test Cases:
# dog1 = Dog("Isis", "Dalmatian", 15)
# dog1.eat("Steak")
# dog1.make_sound()
# dog1.call()

# cat1 = Cat("Puss", "Persian", 4)
# cat1.eat("Tuna")
# cat1.make_sound()
# cat1.call()



from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    @abstractclassmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self.breed = breed
        self.age = age
        self.name = name
    #setters
    def set_name(self, name):
        self.name=name
    def set_breed(self, breed):
        self.breed=breed
    def set_age(self, age):
        self.age=age
    def set_food(self, food):
        self.food=food
        
    #getters
    def get_name(self):
        print(self.name)
    def get_breed(self):
        print(self.breed)
    def get_age(self):
        print(self.age)
    
    #implementation of abstract methods
    def call(self):
        print(f'{self.name}, come on!')
    def eat(self, food):
        print(f'Serve me {food}')
    def make_sound(self):
        print('Miaow! Nyaw! Nyaaaaa!')

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self.breed = breed
        self.age = age
        self.name = name
    #setters
    def set_name(self, name):
        self.name-name
    def set_breed(self, breed):
        self.breed=breed
    def set_age(self, age):
        self.age=age
    def set_food(self, food):
        self.food=food
    #getters
    def get_name(self):
        print(self.name)
    def get_breed(self):
        print(self.breed)
    def get_age(self):
        print(self.age)
    #implementation of abstract methods
    def call(self):
        print(f'Here {self.name}!')
    def eat(self, food):
        print(f'Eaten {food}')
    def make_sound(self):
        print('Bark! Woof! Arf!')

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()
cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()